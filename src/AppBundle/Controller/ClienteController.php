<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Cliente;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Clientedocumento;
use AppBundle\Entity\Clientedireccion;
use AppBundle\Entity\Bitacora;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Cliente controller.
 *
 * @Route("cliente")
 */
class ClienteController extends Controller {

    /**
     * Lists all cliente entities.
     *
     * @Route("/", name="cliente_index")
     * @Method("GET")
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $clientes = $em->getRepository('AppBundle:Cliente')->findAll();

        return $this->render('cliente/index.html.twig', array(
                    
                    'clientes' => $clientes,
        ));
    }

    /**
     * Creates a new cliente entity.
     *
     * @Route("/new", name="cliente_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request) {
        $cliente = new Cliente();
        $form = $this->createForm('AppBundle\Form\ClienteType', $cliente);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $cliente->setIsactivo(true);
            $cliente->setFecharegistro(new \DateTime());
            $em->persist($cliente);
            $this->bitacora($cliente, "New", "Creó Cliente");
            $em->flush();

            return $this->redirectToRoute('cliente_show', array('id' => $cliente->getId()));
        }

        return $this->render('cliente/new.html.twig', array(
                    'cliente' => $cliente,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a cliente entity.
     *
     * @Route("/{id}", name="cliente_show")
     * @Method("GET")
     */
    public function showAction(Cliente $cliente) {
        $em = $this->getDoctrine()->getManager();
        $deleteForm = $this->createDeleteForm($cliente);

        $Direcciones = $em->getRepository("AppBundle:Clientedireccion")->findBy(array('idcliente' => $cliente->getId()));
        $Documentos = $em->getRepository("AppBundle:Clientedocumento")->findBy(array('idcliente' => $cliente->getId()));


        return $this->render('cliente/show.html.twig', array(
                    'cliente' => $cliente,
                    'direcciones' => $Direcciones,
                    'documentos' => $Documentos,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing cliente entity.
     *
     * @Route("/{id}/edit", name="cliente_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Cliente $cliente) {
        $deleteForm = $this->createDeleteForm($cliente);
        $editForm = $this->createForm('AppBundle\Form\ClienteType', $cliente);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->bitacora($cliente, "Edit", "Edito Cliente");
            $this->getDoctrine()->getManager()->flush();


            return $this->redirectToRoute('cliente_index');
        }

        return $this->render('cliente/edit.html.twig', array(
                    'cliente' => $cliente,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a cliente entity.
     *
     * @Route("/{id}", name="cliente_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Cliente $cliente) {
        $form = $this->createDeleteForm($cliente);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($cliente);
            $this->bitacora($cliente, "Delete", "Eliminó Cliente");
            $em->flush();
        }

        return $this->redirectToRoute('cliente_index');
    }

    /**
     * Creates a form to delete a cliente entity.
     *
     * @param Cliente $cliente The cliente entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Cliente $cliente) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('cliente_delete', array('id' => $cliente->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

    /* Direcciones, Documentos, Bitacora */

    /**

     * @Route("/{id}/clienteNuevoDocumento", name="cliente_nuevoDocumento")
     * @Method({"GET", "POST"})
     */
    public function nuevoDocumentoAction(Request $request, Cliente $cliente) {

        $clienteDocumento = new Clientedocumento();

        $form = $this->createForm('AppBundle\Form\ClientedocumentoType', $clienteDocumento);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $clienteDocumento->setFechaagregado(new \DateTime());
            $clienteDocumento->setIdcliente($cliente);
            $em->persist($clienteDocumento);
            $this->bitacora($cliente, "New Doc", "Agregó Nuevo Documento");
            $em->flush();

            return $this->redirectToRoute('cliente_index');
        }

        return $this->render('cliente/nuevoDocumento.html.twig', array(
                    'cliente' => $cliente,
                    'form' => $form->createView(),
        ));
    }

    /**

     * @Route("/{id}/clienteNuevaDireccion", name="cliente_nuevaDireccion")
     * @Method({"GET", "POST"})
     */
    public function nuevaDireccionAction(Request $request, Cliente $cliente) {

        $clienteDireccion = new Clientedireccion();

        $form = $this->createForm('AppBundle\Form\ClientedireccionType', $clienteDireccion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $clienteDireccion->setFechaagregado(new \DateTime());
            $clienteDireccion->setIdcliente($cliente);
            $em->persist($clienteDireccion);
            $this->bitacora($cliente, "New Dir", "Agregó Nueva Direccion");
            $em->flush();

            return $this->redirectToRoute('cliente_index');
        }

        return $this->render('cliente/nuevoDocumento.html.twig', array(
                    'cliente' => $cliente,
                    'form' => $form->createView(),
        ));
    }

    /**

     * @Route("/exportar/clienteDireccion", name="exportarClienteDireccion")
     * @Method({"GET", "POST"})
     */
    public function exportarClienteDireccionAction() {

        $em = $this->getDoctrine()->getManager();

        $getClientes = $em->getRepository("AppBundle:Clientedireccion")->findAll();

        $response = new StreamedResponse();
        $response->setCallback(
                function () use ($getClientes) {
            $handle = fopen('php://output', 'r+');
            foreach ($getClientes as $row) {

                $data = array(
                    $row->getId(),
                    $row->getDireccion(),
                    $row->getIdcliente()->getNombre(),
                    $row->getIdcliente()->getApellido(),
                    $row->getIsprincipal(),
                    $row->getIsactivo()
                );
                fputcsv($handle, $data);
            }
            fclose($handle);
        }
        );


        $response->headers->set('Content-Type', 'application/force-download');
        $response->headers->set('Content-Disposition', 'attachment; filename="export.csv"');
        return $response;
    }
    /**

     * @Route("/exportar/clienteDocumento", name="exportarClienteDocumento")
     * @Method({"GET", "POST"})
     */
    public function exportarClienteDocumentoAction() {

        $em = $this->getDoctrine()->getManager();

        $getClientes = $em->getRepository("AppBundle:Clientedocumento")->findAll();

        $response = new StreamedResponse();
        $response->setCallback(
                function () use ($getClientes) {
            $handle = fopen('php://output', 'r+');
            foreach ($getClientes as $row) {

                $data = array(
                    $row->getId(),
                    $row->getIdtipodocumento()->getDescripcion(),
                    $row->getNumerodocumento(),
                    $row->getIdcliente()->getNombre(),
                    $row->getIdcliente()->getApellido(),
                    $row->getIsprincipal(),
                    $row->getIsactivo()
                );
                fputcsv($handle, $data);
            }
            fclose($handle);
        }
        );


        $response->headers->set('Content-Type', 'application/force-download');
        $response->headers->set('Content-Disposition', 'attachment; filename="export.csv"');
        return $response;
    }

    private function bitacora(Cliente $cliente, $tipoAccion, $accion) {

        $em = $this->getDoctrine()->getManager();
        // $cliente = $em->getRepository("AppBundle:Cliente")->findById($idCliente);
        $usuario = $this->getUser();


        $bitacora = new Bitacora();
        $bitacora->setTipoaccion($tipoAccion);
        $bitacora->setAccion($accion);
        $bitacora->setIdcliente($cliente);
        $bitacora->setIdusuario($usuario);
        $bitacora->setFecha(new \DateTime());

        $em->persist($bitacora);
        $em->flush();
        return false;
    }

}
