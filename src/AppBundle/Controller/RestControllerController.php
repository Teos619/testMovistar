<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Usuario;

/**
 * Cliente controller.
 *
 * @Route("rest")
 */
class RestControllerController extends Controller
{
    
      /**
     * Creates a new cliente entity.
     *
     * @Route("/getBitacora/{id}", name="getBitacora")
     * @Method({"GET", "POST"})
     */
    public function getBitacoraAction($id=null) {
        
        $em = $this->getDoctrine()->getManager();
        
        if(!$id){
         $getBitacora =null;
        }elseif($id=="todos"){
                   $getBitacora = $em->CreateQuery("Select bitacora, cliente, usuario.id, usuario.nombre, usuario.apellido from AppBundle:Bitacora bitacora"
                . " join bitacora.idcliente cliente "
                . " join bitacora.idusuario usuario")->getArrayResult();
        }else{
             $getBitacora = $em->CreateQuery("Select bitacora, cliente, usuario.id, usuario.nombre, usuario.apellido from AppBundle:Bitacora bitacora"
                . " join bitacora.idcliente cliente "
                . " join bitacora.idusuario usuario where usuario.id = :id")->setParameters(array('id'=>$id))->getArrayResult();
        }
        
    
        return new JsonResponse($getBitacora, Response::HTTP_OK);
        
        
        
    }
}
