<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tipodocumento
 *
 * @ORM\Table(name="tipodocumento")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TipodocumentoRepository")
 */
class Tipodocumento
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=50, nullable=false)
     */
    private $descripcion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isActivo", type="boolean", nullable=false)
     */
    private $isactivo;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Tipodocumento
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set isactivo
     *
     * @param boolean $isactivo
     *
     * @return Tipodocumento
     */
    public function setIsactivo($isactivo)
    {
        $this->isactivo = $isactivo;

        return $this;
    }

    /**
     * Get isactivo
     *
     * @return boolean
     */
    public function getIsactivo()
    {
        return $this->isactivo;
    }
}
