<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Clientedocumento
 *
 * @ORM\Table(name="clientedocumento")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ClientedocumentoRepository")
 */
class Clientedocumento
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
      /**
     * @var string
     *
     * @ORM\Column(name="numeroDocumento", type="string", length=100, nullable=false)
     */
    private $numerodocumento;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaAgregado", type="datetime", nullable=false)
     */
    private $fechaagregado;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isPrincipal", type="boolean", nullable=false)
     */
    private $isprincipal;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isActivo", type="boolean", nullable=false)
     */
    private $isactivo;

    /**
     * @var \Cliente
     *
     * @ORM\ManyToOne(targetEntity="Cliente")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idCliente", referencedColumnName="id")
     * })
     */
    private $idcliente;

    /**
     * @var \Tipodocumento
     *
     * @ORM\ManyToOne(targetEntity="Tipodocumento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idTipoDocumento", referencedColumnName="id")
     * })
     */
    private $idtipodocumento;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numerodocumento
     *
     * @param string $numerodocumento
     *
     * @return Clientedocumento
     */
    public function setNumerodocumento($numerodocumento)
    {
        $this->numerodocumento = $numerodocumento;

        return $this;
    }

    /**
     * Get numerodocumento
     *
     * @return string
     */
    public function getNumerodocumento()
    {
        return $this->numerodocumento;
    }

    /**
     * Set fechaagregado
     *
     * @param \DateTime $fechaagregado
     *
     * @return Clientedocumento
     */
    public function setFechaagregado($fechaagregado)
    {
        $this->fechaagregado = $fechaagregado;

        return $this;
    }

    /**
     * Get fechaagregado
     *
     * @return \DateTime
     */
    public function getFechaagregado()
    {
        return $this->fechaagregado;
    }

    /**
     * Set isprincipal
     *
     * @param boolean $isprincipal
     *
     * @return Clientedocumento
     */
    public function setIsprincipal($isprincipal)
    {
        $this->isprincipal = $isprincipal;

        return $this;
    }

    /**
     * Get isprincipal
     *
     * @return boolean
     */
    public function getIsprincipal()
    {
        return $this->isprincipal;
    }

    /**
     * Set isactivo
     *
     * @param boolean $isactivo
     *
     * @return Clientedocumento
     */
    public function setIsactivo($isactivo)
    {
        $this->isactivo = $isactivo;

        return $this;
    }

    /**
     * Get isactivo
     *
     * @return boolean
     */
    public function getIsactivo()
    {
        return $this->isactivo;
    }

    /**
     * Set idcliente
     *
     * @param \AppBundle\Entity\Cliente $idcliente
     *
     * @return Clientedocumento
     */
    public function setIdcliente(\AppBundle\Entity\Cliente $idcliente = null)
    {
        $this->idcliente = $idcliente;

        return $this;
    }

    /**
     * Get idcliente
     *
     * @return \AppBundle\Entity\Cliente
     */
    public function getIdcliente()
    {
        return $this->idcliente;
    }

    /**
     * Set idtipodocumento
     *
     * @param \AppBundle\Entity\Tipodocumento $idtipodocumento
     *
     * @return Clientedocumento
     */
    public function setIdtipodocumento(\AppBundle\Entity\Tipodocumento $idtipodocumento = null)
    {
        $this->idtipodocumento = $idtipodocumento;

        return $this;
    }

    /**
     * Get idtipodocumento
     *
     * @return \AppBundle\Entity\Tipodocumento
     */
    public function getIdtipodocumento()
    {
        return $this->idtipodocumento;
    }
}
