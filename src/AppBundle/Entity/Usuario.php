<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Usuario
 *
 * @ORM\Table(name="usuario")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UsuarioRepository")
 * @UniqueEntity(fields={"username"}, message="Nombre de Usuario ya Existe")
 * @UniqueEntity(fields={"dui"},  message="Dui Ya Existe")
 * @UniqueEntity(fields={"nit"},  message="Nit Ya existe")
 */
class Usuario implements UserInterface, \Serializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=25, unique=true)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=64)
     */
    private $password;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_Activo", type="boolean")
     */
    private $isActivo;
    
        /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=100)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="apellido", type="string", length=100,nullable=true)
     */
    private $apellido;

   
     /**
     * @var array
     *
     * @ORM\Column(name="roles", type="string", length=25 )
     * 
     */
    private $roles;
    


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return Usuario
     */
    public function setUsername($username)
    {
        $this->username = $username;
    
        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return Usuario
     */
    public function setPassword($password)
    {
        $this->password = $password;
    
        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set isActivo
     *
     * @param boolean $isActivo
     *
     * @return Usuario
     */
    public function setIsActivo($isActivo)
    {
        $this->isActive = $isActivo;
    
        return $this;
    }

    /**
     * Get isActivo
     *
     * @return boolean
     */
    public function getIsActivo()
    {
        return $this->isActivo;
    }

    public function eraseCredentials() {
        
    }
       /**
     * Set roles
     *
     * @param string $roles
     * @return Usuarios
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }
     public function setRole($roles)
    {
        $this->roles = $roles;

        return $this;
    }
   public function getRoles() {
        
        return array($this->roles);
    }
    public function getRole(){
         
      return  $this->roles ;
        
    }



    public function getSalt() {
          return null;
    }

    public function serialize(): string {
           return serialize(array(
            $this->id,
            $this->username,
            $this->password,
    
            // see section on salt below
            // $this->salt,
        ));
    }

    public function unserialize($serialized){
         list (
            $this->id,
            $this->username,
            $this->password,

        ) = unserialize($serialized);
        
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Usuario
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellido
     *
     * @param string $apellido
     *
     * @return Usuario
     */
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;
    
        return $this;
    }
    /**
     * Get apellido
     *
     * @return string
     */
    public function getApellido()
    {
        return $this->apellido;
    }

}
