<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Clientedireccion
 *
 * @ORM\Table(name="clientedireccion")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ClientedireccionRepository")
 */
class Clientedireccion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
     /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="text", length=65535, nullable=false)
     */
    private $direccion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaAgregado", type="datetime", nullable=false)
     */
    private $fechaagregado;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isPrincipal", type="boolean", nullable=false)
     */
    private $isprincipal;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isActivo", type="boolean", nullable=false)
     */
    private $isactivo;

    /**
     * @var \Cliente
     *
     * @ORM\ManyToOne(targetEntity="Cliente")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idCliente", referencedColumnName="id")
     * })
     */
    private $idcliente;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     *
     * @return Clientedireccion
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set fechaagregado
     *
     * @param \DateTime $fechaagregado
     *
     * @return Clientedireccion
     */
    public function setFechaagregado($fechaagregado)
    {
        $this->fechaagregado = $fechaagregado;

        return $this;
    }

    /**
     * Get fechaagregado
     *
     * @return \DateTime
     */
    public function getFechaagregado()
    {
        return $this->fechaagregado;
    }

    /**
     * Set isprincipal
     *
     * @param boolean $isprincipal
     *
     * @return Clientedireccion
     */
    public function setIsprincipal($isprincipal)
    {
        $this->isprincipal = $isprincipal;

        return $this;
    }

    /**
     * Get isprincipal
     *
     * @return boolean
     */
    public function getIsprincipal()
    {
        return $this->isprincipal;
    }

    /**
     * Set isactivo
     *
     * @param boolean $isactivo
     *
     * @return Clientedireccion
     */
    public function setIsactivo($isactivo)
    {
        $this->isactivo = $isactivo;

        return $this;
    }

    /**
     * Get isactivo
     *
     * @return boolean
     */
    public function getIsactivo()
    {
        return $this->isactivo;
    }

    /**
     * Set idcliente
     *
     * @param \AppBundle\Entity\Cliente $idcliente
     *
     * @return Clientedireccion
     */
    public function setIdcliente(\AppBundle\Entity\Cliente $idcliente = null)
    {
        $this->idcliente = $idcliente;

        return $this;
    }

    /**
     * Get idcliente
     *
     * @return \AppBundle\Entity\Cliente
     */
    public function getIdcliente()
    {
        return $this->idcliente;
    }
}
