<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ClientedocumentoType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('idtipodocumento', EntityType::class, array(
                    'class' => 'AppBundle:Tipodocumento',
                    'choice_label' => 'descripcion',
                ))->add('numerodocumento')
                //  ->add('fechaagregado')
                ->add('isprincipal')
                ->add('isactivo')
        // ->add('idcliente')

        ;
    }

/**
     * {@inheritdoc}
     */

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Clientedocumento'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'appbundle_clientedocumento';
    }

}
